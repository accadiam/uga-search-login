/*
Called when the item has been created, or when creation failed due to an error.
We'll just log success/failure here.
*/
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

/*
Called when the item has been removed.
We'll just log success here.
*/
function onRemoved() {
  console.log("Item removed successfully");
}

/*
Called when there was an error.
We'll just log the error here.
*/
function onError(error) {
  console.log(`Error: ${error}`);
}


browser.contextMenus.create({
  id: "login-search",
  title: "Recherche login UGA : %s",
  contexts: ["selection"]
});

browser.contextMenus.onClicked.addListener(function(info, tab) {
  if (info.menuItemId == "login-search") {
    console.log("https://help.univ-grenoble-alpes.fr/front/user.php?criteria[0][value]="+info.selectionText);
    browser.windows.create({
      url: "https://help.univ-grenoble-alpes.fr/front/user.php?criteria[0][value]="+info.selectionText
    });
  }
});
